# Get Started #

There is a demo scene: ProceduralToolkitUser/Assets/Personal/Scenes/WallScene.unity

Load it in Unity and have fun!

# About #

This is a library for building rooms in Unity. It provides MonoBehaviours for placing walls and floors, visualizing the build process (outlines), snapping.

# References #

This project uses the Procedural Toolkit library, a great asset in Unity. Find it here: https://assetstore.unity.com/packages/tools/procedural-toolkit-16508
