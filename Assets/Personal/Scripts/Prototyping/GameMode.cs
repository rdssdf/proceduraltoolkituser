﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser
{
	public interface GameMode : IEventSystemHandler
	{
		void Activate();
		void Deactivate();
	}
}
