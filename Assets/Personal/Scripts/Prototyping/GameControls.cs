﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser
{
	enum ViewMode
	{
		FirstPerson, Overview
	}

	public class GameControls : MonoBehaviour
	{
		public GameObject firstPerson;
		public GameObject overview;

		private string toggleViewKey = "return";
		private string exitKey = "escape";
		private ViewMode viewMode;

		private void Start()
		{
			viewMode = ViewMode.Overview;
			UpdateViewMode();
		}

		private void Update()
		{
			if (Input.GetKeyDown(toggleViewKey))
			{
				ToggleViewMode();
				UpdateViewMode();
			}

			if (Input.GetKeyDown(exitKey))
			{
				Application.Quit();
			}
		}

		private void ToggleViewMode()
		{
			viewMode = viewMode == ViewMode.FirstPerson ? ViewMode.Overview : ViewMode.FirstPerson;
		}

		private void UpdateViewMode()
		{
			switch (viewMode)
			{
				case ViewMode.FirstPerson:
					//ExecuteEvents.Execute<GameMode>(overview, null,
					//	(x, y) => x.Deactivate());
					//ExecuteEvents.Execute<GameMode>(firstPerson, null,
					//	(x, y) => x.Activate());

					firstPerson.SetActive(true);
					overview.SetActive(false);
					Cursor.lockState = CursorLockMode.Locked;
					break;
				case ViewMode.Overview:
					//ExecuteEvents.Execute<GameMode>(firstPerson, null,
					//	(x, y) => x.Deactivate());
					//ExecuteEvents.Execute<GameMode>(overview, null,
					//	(x, y) => x.Activate());
					firstPerson.SetActive(false);
					overview.SetActive(true);
					Cursor.lockState = CursorLockMode.None;
					break;
				default:
					throw new Exception("unknown view mode");
			}
		}

	}
}
