﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser
{
	public class FirstPersonWalking : MonoBehaviour
	{
		public Camera firstPersonCamera;

		Vector2 _mouseAbsolute;
		Vector2 _smoothMouse;

		public Vector2 clampInDegrees = new Vector2(360, 180);
		public Vector2 sensitivity = new Vector2(2, 2);
		public Vector2 smoothing = new Vector2(3, 3);
		public Vector3 headPosition;

		public float walkSpeed = 0.5f;
		private GameObject head;
		// head rotates up/down
		private Vector2 headDirection;
		// body rotates left/right
		private Vector2 bodyDirection;

		private void Start()
		{
			head = new GameObject("FirstPersonCamera");
			head.transform.parent = transform;
			head.transform.localPosition = headPosition;
			firstPersonCamera = head.AddComponent<Camera>();
			firstPersonCamera.orthographic = false;

			bodyDirection = transform.localRotation.eulerAngles;
			headDirection = head.transform.localRotation.eulerAngles;
		}

		private void FixedUpdate()
		{
			look();
			move();
		}

		private void look()
		{
			// Get raw mouse input for a cleaner reading on more sensitive mice.
			Vector2 mouseDelta = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

			if (mouseDelta == Vector2.zero)
				return;

			// Scale input against the sensitivity setting and multiply that against the smoothing value.
			mouseDelta = Vector2.Scale(mouseDelta, new Vector2(sensitivity.x * smoothing.x, sensitivity.y * smoothing.y));

			// Interpolate mouse movement over time to apply smoothing delta.
			_smoothMouse.x = Mathf.Lerp(_smoothMouse.x, mouseDelta.x, 1f / smoothing.x);
			_smoothMouse.y = Mathf.Lerp(_smoothMouse.y, mouseDelta.y, 1f / smoothing.y);

			// Find the absolute mouse movement value from point zero.
			_mouseAbsolute += _smoothMouse;

			// Clamp and apply the local x value first, so as not to be affected by world transforms.
			if (clampInDegrees.x < 360)
				_mouseAbsolute.x = Mathf.Clamp(_mouseAbsolute.x, -clampInDegrees.x * 0.5f, clampInDegrees.x * 0.5f);

			// Then clamp and apply the global y value.
			if (clampInDegrees.y < 360)
				_mouseAbsolute.y = Mathf.Clamp(_mouseAbsolute.y, -clampInDegrees.y * 0.5f, clampInDegrees.y * 0.5f);

			var headQuaternion = Quaternion.Euler(headDirection);
			var bodyQuaternion = Quaternion.Euler(bodyDirection);

			head.transform.localRotation = Quaternion.AngleAxis(-_mouseAbsolute.y, headQuaternion * Vector3.right) * headQuaternion;

			var yRotation = Quaternion.AngleAxis(_mouseAbsolute.x, Vector3.up);
			transform.localRotation = yRotation * bodyQuaternion;
		}

		private void move()
		{
			// FIXME diagonal walking is (2 times?) faster than normal
			float leftRightMovement = Input.GetAxis("Horizontal");
			float forwardBackwardMovement = Input.GetAxis("Vertical");
			transform.Translate(new Vector3(leftRightMovement*walkSpeed, 0, forwardBackwardMovement*walkSpeed));
		}

	}
}
