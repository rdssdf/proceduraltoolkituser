﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ToolkitUser.Architect
{
	/// <summary>
	/// A procedural wall generator
	/// </summary>
	public class PrototypeDoorsGenerator : MonoBehaviour
	{
		public float wallLength = 30f;
		public float wallHeight = 10f;
		public Material wallMaterial;
		[Range(0f, 1f)]
		public float relativeDoorPosition = 0.5f;
		public float doorWidth = 3f;
		public float doorHeight = 7f;

		private void Start()
		{
			aligned();
		}

		private void aligned()
		{
			MeshDraft draft = MeshHelper.WallWithDoor(wallLength, wallHeight, 1, doorWidth, doorHeight, relativeDoorPosition);
			GameObject wall = BuildHelper.createGameObject("wall with door", transform);
			BuildHelper.ApplyMeshToGameObject(wall, draft);
			wall.GetComponent<MeshRenderer>().material = wallMaterial;
		}

		private void anyLine(Vector3 from, Vector3 to)
		{

		}

	}
}
