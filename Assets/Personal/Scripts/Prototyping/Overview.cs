﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser
{
	public class Overview : MonoBehaviour
	{
		public Camera overviewCamera;

		private void Start()
		{
			transform.position = new Vector3(-50f, 50f, -50f);
			transform.rotation = Quaternion.LookRotation(new Vector3(45f, 45f, 0));
			overviewCamera.orthographic = true;
		}

	}
}
