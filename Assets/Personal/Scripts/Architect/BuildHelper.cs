﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ToolkitUser.Architect
{
	public static class Layers
	{
		public static readonly string FloorClicking = "FloorClicking";
		public static readonly string PassiveHandles = "PassiveHandles";
		public static readonly string ActiveHandles = "ActiveHandles";
	}

	public static class BuildHelper
	{

		public static GameObject createGameObject(string name, Transform parent)
		{
			GameObject result = new GameObject(name);
			result.transform.parent = parent;
			return result;
		}

		public static void ApplyMeshToGameObject(GameObject result, MeshDraft draft)
		{
			getOrAddComponent<MeshRenderer>(result);
			MeshFilter meshFilter = getOrAddComponent<MeshFilter>(result);
			meshFilter.mesh = draft.ToMesh();
		}

		public static void ApplyMaterialToGameObject(GameObject result, Material material, Color color)
		{
			ApplyMaterialToGameObject(result, material);
			MeshRenderer renderer = getOrAddComponent<MeshRenderer>(result);
			renderer.material.color = color;
		}

		public static void ApplyMaterialToGameObject(GameObject result, Material material)
		{
			MeshRenderer renderer = getOrAddComponent<MeshRenderer>(result);
			renderer.material = material;
		}

		private static T getOrAddComponent<T>(GameObject result) where T : Component
		{
			T component = result.GetComponent<T>();
			return component == null ? result.AddComponent<T>() : component;
		}

	}
}
