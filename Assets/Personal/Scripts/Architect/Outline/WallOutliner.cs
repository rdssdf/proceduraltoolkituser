﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser.Architect
{
	[RequireComponent(typeof(WallGenerator))]
	public class WallOutliner : Outliner
	{
		private float width;
		private float height;

		private void Awake()
		{
			WallGenerator wallGenerator = GetComponent<WallGenerator>();
			width = wallGenerator.wallWidth;
			height = wallGenerator.wallHeight;
		}

		protected override MeshDraft drawOutline(Vector3 origin, Vector3 target)
		{
			MeshDraft draft = MeshHelper.Wall(new WallDef(origin, target, width, height));
			return draft;
		}

		protected override string getName()
		{
			return "wallOutliner";
		}
	}
}
