﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser.Architect
{
	public interface OutlineEventHandler : IEventSystemHandler
	{
		void BeginOutline(Vector3 point);
		void UpdateOutline(Vector3 point);
		void EndOutline();
	}
}
