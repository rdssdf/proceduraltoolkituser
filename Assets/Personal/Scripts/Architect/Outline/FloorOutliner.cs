﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser.Architect
{
	public class FloorOutliner : Outliner
	{
		protected override MeshDraft drawOutline(Vector3 origin, Vector3 target)
		{
			MeshDraft draft = MeshHelper.Plane(origin, target);

			// move to origin. also elevate by 1, so we render above the floor layer
			draft.Move(new Vector3(origin.x, origin.y + 1, origin.z));
			return draft;
		}

		protected override string getName()
		{
			return "floorOutliner";
		}

	}

}
