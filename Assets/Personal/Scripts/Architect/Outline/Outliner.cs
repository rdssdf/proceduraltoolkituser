﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ToolkitUser.Architect
{
	public abstract class Outliner : MonoBehaviour, OutlineEventHandler
	{
		public Material outlineMaterial;

		private GameObject outline;
		private Vector3 origin;
		private MeshFilter meshFilter;

		protected abstract MeshDraft drawOutline(Vector3 origin, Vector3 target);
		protected abstract string getName();

		private void Start()
		{
			outline = new GameObject(getName());
			outline.transform.parent = transform;
			BuildHelper.ApplyMaterialToGameObject(outline, outlineMaterial);
			meshFilter = outline.AddComponent<MeshFilter>();
		}

		public void BeginOutline(Vector3 point)
		{
			origin = point;

			// clear previous mesh
			meshFilter.mesh = new MeshDraft().ToMesh();
			meshFilter.mesh.UploadMeshData(false);

			outline.SetActive(true);
		}

		public void UpdateOutline(Vector3 point)
		{
			MeshDraft draft = drawOutline(origin, point);
			meshFilter.mesh = draft.ToMesh();
			meshFilter.mesh.UploadMeshData(false);
		}

		public void EndOutline()
		{
			outline.SetActive(false);
		}

	}

}
