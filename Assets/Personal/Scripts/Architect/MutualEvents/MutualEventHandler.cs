﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;


namespace ToolkitUser.Architect
{
	public interface MutualEventHandler : IEventSystemHandler
	{
		void SetListening(bool value);
	}
}
