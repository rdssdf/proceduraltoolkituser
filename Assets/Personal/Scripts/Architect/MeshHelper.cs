﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ToolkitUser.Architect
{
	public static class MeshHelper
	{

		public static MeshDraft CenteredPlane(float length, float width)
		{
			MeshDraft draft = MeshDraft.Plane(length, width);
			draft.Move(new Vector3(-length / 2, 0, -width / 2));
			return draft;
		}

		public static MeshDraft Plane(Vector3 from, Vector3 to)
		{
			MeshDraft draft = MeshDraft.Plane(to.x - from.x, to.z - from.z);
			if (isLookingDown(draft))
			{
				draft.FlipTriangles();
			}
			return draft;
		}

		public static MeshDraft WallWithDoor(WallDef wallDef, DoorDef doorDef)
		{
			Vector3 difference = (wallDef.to - wallDef.from);
			float wallLength = difference.magnitude;
			MeshDraft result = WallWithDoor(wallLength, wallDef.height, wallDef.width, doorDef.width, doorDef.height, doorDef.relativePosition);
			result.Rotate(Quaternion.LookRotation(new Vector3(-difference.z, 0, difference.x), Vector3.up));
			result.Move(wallDef.from);
			return result;
		}

		public static MeshDraft Wall(WallDef wallDef)
		{
			Vector3 difference = (wallDef.to - wallDef.from);
			float length = difference.magnitude;
			MeshDraft draft = MeshDraft.Hexahedron(wallDef.width, length, wallDef.height);
			draft.Move(Vector3.up * wallDef.height / 2);
			draft.Rotate(Quaternion.LookRotation(difference, Vector3.up));
			draft.Move((wallDef.from + wallDef.to) / 2);
			return draft;
		}

		public static MeshDraft WallWithDoor(float wallLength, float wallHeight, float wallWidth, float doorWidth, float doorHeight, float relativeDoorPosition)
		{
			if (relativeDoorPosition < 0 || relativeDoorPosition > 1f)
				throw new Exception();

			bool requiresLeft = relativeDoorPosition > 0;
			bool requiresRight = relativeDoorPosition < 1;
			bool requiresUpper = doorHeight < wallHeight;

			MeshDraft result = new MeshDraft();

			if (requiresUpper)
			{
				MeshDraft upper = Wall(new WallDef(new Vector3(0, doorHeight, 0), new Vector3(wallLength, doorHeight, 0), wallWidth, wallHeight - doorHeight));
				result.Add(upper);
			}

			var availableDistance = wallLength - doorWidth;
			if (availableDistance <= 0)
			{
				return result;
			}

			// To map
			// [A, B] --> [a, b]
			// use this formula
			// (val - A) * (b - a) / (B - A) + a

			// [0, 1] --> [0, availableDistance]
			// (relativeDoorPosition - 0) * (availabeDistance - 0) / (1 - 0) + 0
			var localPosition = relativeDoorPosition * availableDistance;

			float trimmedDoorHeight = requiresUpper ? doorHeight : wallHeight;

			if (requiresLeft)
			{
				MeshDraft left = Wall(new WallDef(Vector3.zero, new Vector3(localPosition, 0, 0), wallWidth, trimmedDoorHeight));
				result.Add(left);
			}

			if(requiresRight)
			{
				MeshDraft right = Wall(new WallDef(new Vector3(localPosition+doorWidth, 0, 0), new Vector3(wallLength, 0, 0), wallWidth, trimmedDoorHeight));
				result.Add(right);
			}

			return result;
		}

		/// <summary>
		/// Returns true if at least one triangle in the mesh is looking down.
		/// </summary>
		public static bool isLookingDown(MeshDraft draft)
		{
			List<int> triangles = draft.triangles;
			if(triangles.Count % 3 != 0)
				throw new Exception("bad triangles");

			foreach (Vector3[] triangle in yieldTriangles(draft))
			{
				if (isTriangleLookingDown(triangle))
					return true;
			}

			return false;
		}

		private static bool isTriangleLookingDown(Vector3[] triangle)
		{
			Vector3 sideA = triangle[0] - triangle[1];
			Vector3 sideC = triangle[2] - triangle[1];

			Vector3 normal = Vector3.Cross(sideA, sideC);
			return normal.y > 0;
		}

		public static IEnumerable<Vector3[]> yieldTriangles(MeshDraft draft)
		{
			List<Vector3> vertices = draft.vertices;
			List<int> triangles = draft.triangles;

			for (int offset = 0; offset < triangles.Count; offset+=3)
			{
				Vector3[] result = {
					vertices[triangles[offset + 0]],
					vertices[triangles[offset + 1]],
					vertices[triangles[offset + 2]]
				};

				yield return result;
			}
		}

	}

	public struct WallDef
	{
		public Vector3 from;
		public Vector3 to;
		public float width;
		public float height;

		public WallDef(Vector3 from, Vector3 to, float width, float height)
		{
			this.from = from;
			this.to = to;
			this.width = width;
			this.height = height;
		}
	}

	public struct DoorDef
	{
		public float relativePosition;
		public float width;
		public float height;

		/// <summary>
		/// relativePosition: A value between 0 and 1, where 0 means the door is located at the wall's source node and
		/// 1 means it is located at the door's destination node (minus the width of the door). 0.5 would be in the center.
		/// </summary>
		public DoorDef(float relativePosition, float width, float height)
		{
			this.relativePosition = relativePosition;
			this.width = width;
			this.height = height;
		}
	}
}
