﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser.Architect
{
	[RequireComponent(typeof(MeshCollider))]
	public class BasicFloorClicking : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, DragCollisionEventHandler
	{
		public float length;
		public float width;
		public Material wallMaterial;
		public Material floorMaterial;
		public Material areaMaterial;

		private BuildTarget buildTarget = BuildTarget.Wall;

		public enum BuildTarget { Wall, Floor };

		private void Start()
		{
			MeshDraft clickableArea = MeshHelper.CenteredPlane(length, width);
			MeshCollider collider = GetComponent<MeshCollider>();
			collider.sharedMesh = clickableArea.ToMesh();

			/** This does not feel right...
			 *  Why does GameObject areaVisualization need to be in Layer.FloorClicking?
			 *  Shouldn't this.gameObject be in Layer.FloorClicking? After all, the MeshCollider sits on this object.
			 *  But then the camera raycasts don't hit anymore. */
			clickableArea.Move(new Vector3(0, -1f, 0));
			GameObject areaVisualization = BuildHelper.createGameObject("belowClickableArea", transform);
			areaVisualization.layer = LayerMask.NameToLayer(Layers.FloorClicking);
			/** **************************** */

			BuildHelper.ApplyMeshToGameObject(areaVisualization, clickableArea);
			BuildHelper.ApplyMaterialToGameObject(areaVisualization, areaMaterial);

			// do this before updateTargetComponents() when all components
			// are active and can receive events
			ExecuteEvents.Execute<WallGenerator>(gameObject, null,
				(x, y) => x.UseMaterial(wallMaterial));
			ExecuteEvents.Execute<FloorGenerator>(gameObject, null,
				(x, y) => x.UseMaterial(floorMaterial));

			updateTargetComponents();
		}

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.B))
			{
				toggleBuildTarget();
				updateTargetComponents();
			}
		}

		private void toggleBuildTarget()
		{
			buildTarget = buildTarget == BuildTarget.Floor ? BuildTarget.Wall : BuildTarget.Floor;
		}

		private void updateTargetComponents()
		{
			bool wallGeneratorFlag = buildTarget == BuildTarget.Wall;
			GetComponent<WallGenerator>().enabled = wallGeneratorFlag;
			GetComponent<WallOutliner>().enabled = wallGeneratorFlag;

			bool floorGeneratorFlag = buildTarget == BuildTarget.Floor;
			GetComponent<FloorGenerator>().enabled = floorGeneratorFlag;
			GetComponent<FloorOutliner>().enabled = floorGeneratorFlag;
		}

		private bool haveCollision;
		private Vector3 colliderPosition;

		public void OnEnterCollider(Vector3 colliderPosition)
		{
			haveCollision = true;
			this.colliderPosition = colliderPosition;
		}

		public void OnLeaveCollider()
		{
			haveCollision = false;
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			Vector3 transformedPressed = getHitPosition(eventData.pressPosition);
			ExecuteEvents.Execute<OutlineEventHandler>(gameObject, null,
				(x, y) => x.BeginOutline(transformedPressed));
			ExecuteEvents.Execute<PointerMovementEventHandler>(gameObject, null,
				(x, y) => x.InitialPosition(transformedPressed));
		}

		public void OnDrag(PointerEventData eventData)
		{
			// pass the true raycast hit position to the active collider
			Vector3 pointerPosition = getHitPosition(eventData.position);
			ExecuteEvents.Execute<PointerMovementEventHandler>(gameObject, null,
				(x, y) => x.UpdatePosition(pointerPosition));

			// pass either the stored position of the other collider or the raycast position
			// to the wall/floor outline
			Vector3 outlinePosition = haveCollision ? colliderPosition : pointerPosition;
			ExecuteEvents.Execute<OutlineEventHandler>(gameObject, null,
				(x, y) => x.UpdateOutline(outlinePosition));
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			ExecuteEvents.Execute<OutlineEventHandler>(gameObject, null,
				(x, y) => x.EndOutline());

			Vector3 transformedPressed = getHitPosition(eventData.pressPosition);
			Vector3 transformedReleased = haveCollision ? colliderPosition : getHitPosition(eventData.position);
			haveCollision = false;

			ExecuteEvents.Execute<LineEventHandler>(gameObject, null,
				(x, y) => x.Line(transformedPressed, transformedReleased));

			ExecuteEvents.Execute<PointerMovementEventHandler>(gameObject, null,
				(x, y) => x.EndMovement());
		}

		private Vector3 getHitPosition(Vector3 mousePosition)
		{
			Ray ray = Camera.main.ScreenPointToRay(mousePosition);
			int clickLayer = LayerMask.NameToLayer(Layers.FloorClicking);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 1000f, clickLayer))
			{
				return hit.point;
			}

			throw new Exception("mouse pointer out of bounds");
		}

	}
}
