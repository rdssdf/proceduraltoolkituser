﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ToolkitUser.Architect
{
	/// <summary>
	/// A procedural wall generator
	/// </summary>
	public class WallWithDoorsGenerator : WallGenerator
	{
		public float doorWidth;
		public float doorHeight;
		public bool randomDoorPosition;

		private LineCache lineCache = new LineCache();

		public override GameObject Line(Vector3 from, Vector3 to)
		{
			float distance = Vector3.Distance(from, to);
			bool isWallLongEnoughForDoor = doorWidth < distance;

			if (isWallLongEnoughForDoor)
			{
				float relativeDoorPosition;

				// Random door position has a memory leak. The cache is never cleaned,
				// so the edges which have changed with the last added site stay forever.
				if (randomDoorPosition)
				{
					if(!lineCache.Contains(from, to))
					{
						relativeDoorPosition = UnityEngine.Random.value;
						lineCache.Put(from, to, relativeDoorPosition);
					}
					else
					{
						relativeDoorPosition = lineCache.Get(from, to);
					}


				} else
				{
					relativeDoorPosition = 0.5f;
				}

				DoorDef doorDef = new DoorDef(relativeDoorPosition, doorWidth, doorHeight);
				WallDef wallDef = new WallDef(from, to, wallWidth, wallHeight);
				MeshDraft draft = MeshHelper.WallWithDoor(wallDef, doorDef);

				GameObject wall = BuildHelper.createGameObject("wall_" + wallsContainer.transform.childCount, wallsContainer.transform);
				BuildHelper.ApplyMeshToGameObject(wall, draft);
				wall.GetComponent<MeshRenderer>().material = wallMaterial;

				return wall;
			}
			else
			{
				// otherwise draw a normal wall without doors
				return base.Line(from, to);
			}
		}

		private class LineCache
		{
			private Dictionary<Vector3, Dictionary<Vector3, float>> cache = new Dictionary<Vector3, Dictionary<Vector3, float>>();

			public void Put(Vector3 a, Vector3 b, float value)
			{
				if (!cache.ContainsKey(a))
					cache.Add(a, new Dictionary<Vector3, float>());

				cache[a].Add(b, value);
			}

			public bool Contains(Vector3 a, Vector3 b)
			{
				if (!cache.ContainsKey(a))
					return false;

				return cache[a].ContainsKey(b);
			}

			public float Get(Vector3 a, Vector3 b)
			{
				if (!Contains(a, b))
					throw new Exception("key not found");

				return cache[a][b];
			}
		}

	}
}
