﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ToolkitUser.Architect
{
	/// <summary>
	/// A procedural wall generator
	/// </summary>
	public class WallGenerator : MonoBehaviour, LineEventHandler
	{
		public float wallWidth = 1;
		public float wallHeight = 9;

		protected Material wallMaterial;

		protected GameObject wallsContainer;

		protected virtual void Awake()
		{
			wallsContainer = BuildHelper.createGameObject("walls", transform);
		}

		public virtual GameObject Line(Vector3 from, Vector3 to)
		{
			MeshDraft draft = MeshHelper.Wall(new WallDef(from, to, wallWidth, wallHeight));

			GameObject wall = BuildHelper.createGameObject("wall_" + wallsContainer.transform.childCount, wallsContainer.transform);
			BuildHelper.ApplyMeshToGameObject(wall, draft);
			wall.GetComponent<MeshRenderer>().material = wallMaterial;
			return wall;
		}

		public void ClearAll()
		{
			foreach (Transform child in wallsContainer.transform)
			{
				Destroy(child.gameObject);
			}
			wallsContainer.transform.DetachChildren();
		}

		public void UseMaterial(Material material)
		{
			wallMaterial = material;
		}
	}
}
