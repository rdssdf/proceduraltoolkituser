﻿using System;
using System.Collections.Generic;
using System.Text;
using ProceduralToolkit;
using UnityEngine;
using ToolkitUser.Geometry;
using Prisms;

namespace ToolkitUser.Architect
{
	/// <summary>
	/// Creates squared floors with diagonal corner points from and to.
	/// </summary>
	public class FloorGenerator : MonoBehaviour, LineEventHandler, PolygonEventHandler
	{
		private Material floorMaterial;
		private GameObject floorsContainer;

		protected virtual void Awake()
		{
			floorsContainer = BuildHelper.createGameObject("floors", transform);
		}

		public void UseMaterial(Material material)
		{
			floorMaterial = material;
		}

		public virtual GameObject Line(Vector3 from, Vector3 to)
		{
			MeshDraft draft = MeshHelper.Plane(from, to);
			GameObject floor = CreateFloorObject(draft);
			floor.transform.localPosition = from;
			return floor;
		}

		private GameObject CreateFloorObject(MeshDraft draft)
		{
			GameObject floor = BuildHelper.createGameObject(
				"floor_" + floorsContainer.transform.childCount, floorsContainer.transform);
			BuildHelper.ApplyMeshToGameObject(floor, draft);
			BuildHelper.ApplyMaterialToGameObject(floor, floorMaterial);
			return floor;
		}

		public void ClearAll()
		{
			foreach (Transform child in floorsContainer.transform)
			{
				Destroy(child.gameObject);
			}
			floorsContainer.transform.DetachChildren();
		}

		public virtual GameObject Polygon(List<Vector3> vertices)
		{
			Polygon polygon = Prisms.Polygon.From2D(vertices);
			MeshDraft draft = PrismBuilder.Face(polygon, 0, Facing.Top);
			GameObject floor = CreateFloorObject(draft);
			floor.transform.localPosition = Vector3.zero;
			return floor;
		}
	}
}
