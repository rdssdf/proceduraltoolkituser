﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser.Architect
{
	interface BuildEventHandler : IEventSystemHandler
	{
		void UseMaterial(Material material);
		void ClearAll();
	}

	interface LineEventHandler : BuildEventHandler
	{
		GameObject Line(Vector3 from, Vector3 to);
	}

	interface PolygonEventHandler : BuildEventHandler
	{
		GameObject Polygon(List<Vector3> vertices);
	}
}
