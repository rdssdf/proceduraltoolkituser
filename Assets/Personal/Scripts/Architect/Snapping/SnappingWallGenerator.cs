﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ToolkitUser.Architect
{
	/// <summary>
	/// A procedural wall generator with snapping feature.
	/// </summary>
	public class SnappingWallGenerator : WallGenerator
	{
		public override GameObject Line(Vector3 from, Vector3 to)
		{
			GameObject wall = base.Line(from, to);

			buildHandle("from", from, wall.transform);
			buildHandle("to", to, wall.transform);

			return wall;
		}

		private void buildHandle(string name, Vector3 point, Transform parent)
		{
			GameObject handle = GetComponent<SnapHandleGenerator>().createSnapHandle(name);
			handle.transform.parent = parent;
			handle.transform.localPosition = point;
			handle.layer = LayerMask.NameToLayer(Layers.PassiveHandles);
		}

	}
}
