﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser.Architect
{
	public class ActiveCollider : MonoBehaviour
	{
		public GameObject floorClicking;

		private void OnTriggerEnter(Collider other)
		{
			//print("enter trigger: " + debugTrigger(other));
			ExecuteEvents.Execute<DragCollisionEventHandler>(floorClicking, null,
				(x, y) => x.OnEnterCollider(other.gameObject.transform.position));
		}

		private void OnTriggerExit(Collider other)
		{
			//print("exit trigger: " + debugTrigger(other));
			ExecuteEvents.Execute<DragCollisionEventHandler>(floorClicking, null,
				(x, y) => x.OnLeaveCollider());
		}

		private string debugTrigger(Collider other)
		{
			string name = gameObject.name;
			string otherName = other.gameObject.name;

			string layer = LayerMask.LayerToName(gameObject.layer);
			string otherLayer = LayerMask.LayerToName(other.gameObject.layer);

			Vector3 position = transform.position;
			Vector3 otherPosition = other.transform.position;

			return debugTrigger(name, position, layer) + " ... with: " + debugTrigger(otherName, otherPosition, otherLayer);
		}

		string debugTrigger(string name, Vector3 position, string layer)
		{
			return name + " at " + position + " in layer " + layer;
		}

	}
}
