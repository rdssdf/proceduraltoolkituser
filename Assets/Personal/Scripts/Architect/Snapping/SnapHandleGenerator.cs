﻿using System;
using System.Collections.Generic;
using System.Text;
using ProceduralToolkit;
using UnityEngine;

namespace ToolkitUser.Architect
{
	public class SnapHandleGenerator : MonoBehaviour
	{
		private MeshDraft handleShape = MeshDraft.Sphere(10f, 8, 8);

		public GameObject createSnapHandle(string name)
		{
			GameObject result = new GameObject();
			result.name = name;

			result.AddComponent<Rigidbody>().useGravity = false;
			MeshCollider collider = result.AddComponent<MeshCollider>();
			collider.sharedMesh = handleShape.ToMesh();
			collider.convex = true;
			collider.isTrigger = true;

			return result;
		}

	}
}
