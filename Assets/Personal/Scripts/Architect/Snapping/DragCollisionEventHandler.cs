﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser.Architect
{
	public interface DragCollisionEventHandler : IEventSystemHandler
	{
		/// <summary>
		/// Called when this gameObject's collider enters another collider.
		/// Passes the other collider's position.
		/// </summary>
		void OnEnterCollider(Vector3 colliderPosition);

		/// <summary>
		/// Called when this gameObject's collider leaves another collider.
		/// (You don't know which one.)
		/// </summary>
		void OnLeaveCollider();
	}
}
