﻿using System;
using System.Collections.Generic;
using System.Text;
using ProceduralToolkit;
using UnityEngine;

namespace ToolkitUser.Architect
{
	/// <summary>
	/// Creates squared floors with diagonal corner points from and to.
	/// </summary>
	public class SnappingFloorGenerator : FloorGenerator
	{
		public override GameObject Line(Vector3 from, Vector3 to)
		{
			GameObject floor = base.Line(from, to);
			addHandles(floor.transform, from, to);
			return floor;
		}

		private void addHandles(Transform parent, Vector3 from, Vector3 to)
		{
			MeshDraft draft = MeshHelper.Plane(from, to);

			for (int i = 0; i < draft.vertices.Count; i++)
			{
				GameObject handle = GetComponent<SnapHandleGenerator>().createSnapHandle("handle_" + i);
				handle.transform.parent = parent;
				handle.transform.localPosition = draft.vertices[i];
				handle.layer = LayerMask.NameToLayer(Layers.PassiveHandles);
			}
		}

	}
}
