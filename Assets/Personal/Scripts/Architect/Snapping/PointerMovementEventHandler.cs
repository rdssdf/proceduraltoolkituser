﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser.Architect
{
	interface PointerMovementEventHandler : IEventSystemHandler
	{
		void InitialPosition(Vector3 point);

		void UpdatePosition(Vector3 point);

		void EndMovement();
	}
}
