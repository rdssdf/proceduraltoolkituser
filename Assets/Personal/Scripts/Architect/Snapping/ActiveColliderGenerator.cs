﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ToolkitUser.Architect
{
	[RequireComponent(typeof(SnapHandleGenerator))]
	public class ActiveColliderGenerator : MonoBehaviour, PointerMovementEventHandler
	{
		private GameObject handleCollider;

		private void Start()
		{
			SnapHandleGenerator snapHandleGenerator = gameObject.GetComponent<SnapHandleGenerator>();
			handleCollider = snapHandleGenerator.createSnapHandle("activeCollider");
			handleCollider.transform.parent = transform;
			handleCollider.SetActive(false);
			handleCollider.layer = LayerMask.NameToLayer(Layers.ActiveHandles);

			ActiveCollider activeCollider = handleCollider.AddComponent<ActiveCollider>();
			activeCollider.floorClicking = gameObject;
		}

		public void InitialPosition(Vector3 point)
		{
			handleCollider.transform.localPosition = point;
			handleCollider.SetActive(true);
		}

		public void UpdatePosition(Vector3 point)
		{
			handleCollider.transform.localPosition = point;
		}

		public void EndMovement()
		{
			handleCollider.SetActive(false);
		}

	}

}
