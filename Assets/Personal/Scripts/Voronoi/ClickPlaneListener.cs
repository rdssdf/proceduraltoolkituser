﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using ToolkitUser.Voronoi.Events;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser.Events
{
	// not used yet. the idea is to factor this behavior out from basic voronoi and floor clicking
	[RequireComponent(typeof(MeshCollider))]
	public class ClickPlaneListener : MonoBehaviour, IPointerClickHandler, IPointerDownHandler
	{
		public GameObject voronoiEventListener;

		public void OnPointerClick(PointerEventData eventData)
		{
			Vector3 transformedPressed = getHitPosition(eventData.pressPosition);
			ExecuteEvents.Execute<SiteEventListener>(gameObject, null,
				(x, y) => x.AddSite(transformedPressed));
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			Vector3 transformedPressed = getHitPosition(eventData.pressPosition);
			ExecuteEvents.Execute<SiteEventListener>(voronoiEventListener, null,
				(x, y) => x.AddSite(transformedPressed));
		}

		private Vector3 getHitPosition(Vector3 mousePosition)
		{
			Ray ray = Camera.main.ScreenPointToRay(mousePosition);
			//int clickLayer = LayerMask.NameToLayer(Layers.FloorClicking);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 1000f/*, clickLayer*/))
			{
				return hit.point;
			}

			throw new Exception("mouse pointer out of bounds");
		}
	}
}
