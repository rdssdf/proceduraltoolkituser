﻿using System;
using System.Collections.Generic;
using System.Text;
using ProceduralToolkit;
using UnityEngine;
using ToolkitUser.Architect;

namespace ToolkitUser.Geometry
{
	public enum WindingOrder
	{
		CLOCKWISE, COUNTERCLOCKWISE, NONE
	}

	public static class DrawingUtils
	{
		public static List<Vector3> RandomPointsInRectangle(int numberOfPoints, float width, float height)
		{
			List<Vector3> points = new List<Vector3>();
			for (int i = 0; i < numberOfPoints; i++)
			{
				points.Add(new Vector3(UnityEngine.Random.Range(0, width), 0, UnityEngine.Random.Range(0, height)));
			}

			return points;
		}

		public static float RandomPointInRange(float from, float to)
		{
			return to < from ? UnityEngine.Random.Range(to, from) : UnityEngine.Random.Range(from, to);
		}

		public static List<Vector3> RandomPointsInRectangle(int numberOfPoints, Rect rectangle)
		{
			List<Vector3> points = new List<Vector3>();
			for (int i = 0; i < numberOfPoints; i++)
			{
				float randomX = RandomPointInRange(rectangle.xMin, rectangle.xMax);
				float randomZ = RandomPointInRange(rectangle.yMin, rectangle.yMax);
				points.Add(new Vector3(randomX, 0, randomZ));
			}
			return points;
		}

		public static int CompareVectorsXZ(Vector3 a, Vector3 b)
		{
			if (a.x < b.x)
				return -1;
			else if (a.x > b.x)
				return 1;
			else
			{
				if (a.z < b.z)
					return -1;
				else if (a.z > b.z)
					return 1;
				else
					return 0;
			}
		}

		public static List<Vector3> ConvexHull(List<Vector3> points)
		{
			List<Vector3> workingList = new List<Vector3>(points);

			if (workingList.Count < 2)
				return workingList;

			// Sort by x and y coordinate
			workingList.Sort(CompareVectorsXZ);

			List<Vector3> result = new List<Vector3>();

			result.Add(workingList[0]);
			result.Add(workingList[1]);

			if (workingList.Count <= 2)
				return result;

			// Add upper hull
			for(int i = 2; i < workingList.Count; i++)
			{
				result.Add(workingList[i]);
				removeMiddleVertices(result);
			}

			// Add lower hull
			for(int i = workingList.Count - 1; i >= 1; i--)
			{
				if (result.Contains(workingList[i]))
					continue;

				result.Add(workingList[i]);
				removeMiddleVertices(result);
			}

			// Handle the last hull vertex. 
			// The loops did not compare it to the first hull vertex, so we have to do it manually.
			// Otherwise the result may be concave.
			result.Add(workingList[0]);
			removeMiddleVertices(result);
			result.RemoveAt(result.Count - 1);

			return result;
		}

		private static void removeMiddleVertices(List<Vector3> result)
		{
			while (result.Count > 2 && 
				GetWindingOrder(result[result.Count - 3], result[result.Count - 2], result[result.Count - 1]) != WindingOrder.CLOCKWISE)
			{
				result.RemoveAt(result.Count - 2);
			}
		}

		public static WindingOrder GetWindingOrder(Vector3 a, Vector3 b, Vector3 c)
		{
			Vector3 directionAB = Direction(a, b);
			Vector3 directionBC = Direction(b, c);
			float determinant = Determinant(directionAB, directionBC);
			if (determinant < 0)
				return WindingOrder.CLOCKWISE;
			else if (determinant > 0)
				return WindingOrder.COUNTERCLOCKWISE;
			else
				return WindingOrder.NONE;
		}

		public static Vector3 Direction(Vector3 from, Vector3 to)
		{
			Vector3 heading = to - from;
			float distance = heading.magnitude;
			Vector3 direction = heading / distance;
			return direction;
		}

		public static float Determinant(Vector3 a, Vector3 b)
		{
			return a.x * b.z - a.z * b.x;
		}

		public static Color RandomColor(float alpha=1f)
		{
			Color result = UnityEngine.Random.ColorHSV();
			return result;
		}
	}
}
