﻿using ProceduralToolkit;
using System;
using System.Collections.Generic;
using System.Text;
using ToolkitUser.Geometry;
using ToolkitUser.Voronoi;
using ToolkitUser.Voronoi.Events;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser.Architect
{
	/// <summary>
	/// TODO make this usable by both classes that use their own (but identical) implementation
	/// A mesh collider is required to receive mouse click events
	/// </summary>
	[RequireComponent(typeof(MeshCollider))]
	public class BasicVoronoiClicking : MonoBehaviour, IPointerDownHandler, MutualEventHandler
	{
		public float width = 400;
		public float height = 400;
		public int initialSites;
		private bool listening = true;

		private void Start()
		{
			MeshDraft clickableArea = MeshHelper.CenteredPlane(width, height);
			MeshCollider collider = GetComponent<MeshCollider>();
			collider.sharedMesh = clickableArea.ToMesh();

			var halfwidth = width / 2;
			var halftheight = height / 2;
			Rect bounds = Rect.MinMaxRect(-halfwidth, -halftheight, halfwidth, halftheight);
			PlanarVoronoiEngine engine = new CsDelaunayEngine(bounds);
			ExecuteEvents.Execute<IncrementalVoronoi>(gameObject, null,
				(x, y) => x.SetEngine(engine));

			if (initialSites > 0)
			{
				List<Vector3> points = DrawingUtils.RandomPointsInRectangle(initialSites, bounds);
				ExecuteEvents.Execute<SiteEventListener>(gameObject, null,
					(x, y) => x.AddSites(points));
			}
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			if (!listening)
				return;

			Vector3 transformedPressed = getHitPosition(eventData.pressPosition);
			ExecuteEvents.Execute<SiteEventListener>(gameObject, null,
				(x, y) => x.AddSite(transformedPressed));
		}

		private Vector3 getHitPosition(Vector3 mousePosition)
		{
			Ray ray = Camera.main.ScreenPointToRay(mousePosition);
			int clickLayer = LayerMask.NameToLayer(Layers.FloorClicking);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 1000f, clickLayer))
			{
				return hit.point;
			}

			throw new Exception("mouse pointer out of bounds");
		}

		public void SetListening(bool value)
		{
			listening = value;
		}
	}
}
