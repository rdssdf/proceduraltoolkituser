﻿using System;
using System.Collections.Generic;
using System.Text;
using ProceduralToolkit;
using UnityEngine;
using ToolkitUser.Architect;
using ToolkitUser.Events;
using ToolkitUser.Voronoi.Events;
using UnityEngine.EventSystems;
using ToolkitUser.Geometry;

namespace ToolkitUser.Voronoi
{
	public class IncrementalVoronoi : SiteBasedGenerator, SiteEventListener
	{
		public Material floorMaterial;
		public Material wallMaterial;
		public bool drawEdges = true;
		public bool drawFloor = false;
		public bool drawBounds = true;
		// private and zero atm because it doesn't work with cached site positions
		// The problem is that regions are considered unique by their site location.
		// In fact they can only be unique by some external id, such as insertion order,
		// because Lloyd's relaxation changes the site position.
		private int lloydRelaxations = 0;

		private PlanarVoronoiEngine engine;
		private Dictionary<Vector3, Color> colors = new Dictionary<Vector3, Color>();

		protected virtual void Start()
		{
			if (drawBounds)
			{
				GameObject boundsContainer = new GameObject("bounds");
				boundsContainer.transform.parent = generatedMeshes.transform;
				WallGenerator boundsGenerator = boundsContainer.AddComponent<WallGenerator>();
				boundsGenerator.UseMaterial(Resources.Load<Material>("Materials/Wall"));

				List<Vector3> bounds = engine.GetBounds();
				boundsGenerator.Line(bounds[bounds.Count - 1], bounds[0]);
				for (int i = 1; i < bounds.Count; i++)
				{
					boundsGenerator.Line(bounds[i - 1], bounds[i]);
				}
			}
		}

		public void SetEngine(PlanarVoronoiEngine engine)
		{
			this.engine = engine;
		}

		public override void AddSites(List<Vector3> sites)
		{
			foreach(Vector3 location in sites)
			{
				Debug.Log("add site at " + location);
				points.Add(location);
				colors.Add(location, DrawingUtils.RandomColor());

				drawSite(location);
			}

			if (points.Count > 1)
			{
				ExecuteEvents.Execute<BuildEventHandler>(generatedMeshes, null,
					(x, y) => x.ClearAll());
				VoronoiDiagram diagram = engine.InitializeDiagram(points, lloydRelaxations);

				drawDiagram(diagram);
			}
			else if(points.Count == 1)
			{
				Material material = new Material(floorMaterial);
				material.color = colors[points[0]];

				ExecuteEvents.Execute<FloorGenerator>(generatedMeshes, null,
					(x, y) => x.UseMaterial(material));
				ExecuteEvents.Execute<FloorGenerator>(generatedMeshes, null,
					(x, y) => x.Polygon(engine.GetBounds()));
			}
		}

		public override void AddSite(Vector3 location)
		{
			AddSites(new List<Vector3> { location });
		}

		private void drawDiagram(VoronoiDiagram diagram)
		{
			// for now, all walls use the same material
			ExecuteEvents.Execute<WallGenerator>(generatedMeshes, null,
				(x, y) => x.UseMaterial(wallMaterial));

			if (drawEdges)
				DrawEdges(diagram);

			if (drawFloor)
				DrawFloors(diagram);
		}

		void DrawEdges(VoronoiDiagram diagram)
		{
			foreach (VoronoiEdge edge in diagram.Edges)
			{
				ExecuteEvents.Execute<WallGenerator>(generatedMeshes, null,
					(x, y) => x.Line(edge.From, edge.To));
			}
		}

		void DrawFloors(VoronoiDiagram diagram)
		{
			foreach (VoronoiSite site in diagram.Sites)
			{
				Material material = new Material(floorMaterial);
				material.color = colors[site.Location];

				ExecuteEvents.Execute<FloorGenerator>(generatedMeshes, null,
					(x, y) => x.UseMaterial(material));
				ExecuteEvents.Execute<FloorGenerator>(generatedMeshes, null,
					(x, y) => x.Polygon(site.BoundedRegion));
			}
		}

	}
}
