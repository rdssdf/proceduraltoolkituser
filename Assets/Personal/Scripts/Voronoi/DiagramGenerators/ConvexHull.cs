﻿using System;
using System.Collections.Generic;
using System.Text;
using ProceduralToolkit;
using UnityEngine;
using ToolkitUser.Architect;
using ToolkitUser.Events;
using ToolkitUser.Voronoi.Events;
using UnityEngine.EventSystems;
using ToolkitUser.Geometry;

namespace ToolkitUser.Voronoi
{
	public class ConvexHull : SiteBasedGenerator
	{
		public Material wallMaterial;

		private void Start()
		{
			ExecuteEvents.Execute<WallGenerator>(generatedMeshes, null,
				(x, y) => x.UseMaterial(wallMaterial));

			initialize();
		}

		private void initialize()
		{
			AddSite(new Vector3(-14.68606f, 0, 80.30093f));
			AddSite(new Vector3(-43.3297f, 0, -32.12632f));
			AddSite(new Vector3(-32.68402f, 0, -53.43396f));
			AddSite(new Vector3(-68.66205f, 0, -38.21261f));
		}

		public override void AddSite(Vector3 location)
		{
			AddSites(new List<Vector3> { location });
		}

		public override void AddSites(List<Vector3> sites)
		{
			foreach(Vector3 location in sites)
			{
				Debug.Log("add site at " + location);
				points.Add(location);

				drawSite(location);
			}

			if (points.Count > 1)
			{
				ExecuteEvents.Execute<WallGenerator>(generatedMeshes, null,
					(x, y) => x.ClearAll());

				List<Vector3> hull = DrawingUtils.ConvexHull(points);

				for (int i = 1; i < hull.Count; i++)
				{
					ExecuteEvents.Execute<WallGenerator>(generatedMeshes, null,
						(x, y) => x.Line(hull[i - 1], hull[i]));
				}

				if (points.Count > 2)
				{
					ExecuteEvents.Execute<WallGenerator>(generatedMeshes, null,
						(x, y) => x.Line(hull[hull.Count - 1], hull[0]));
				}
			}
		}
	}
}
