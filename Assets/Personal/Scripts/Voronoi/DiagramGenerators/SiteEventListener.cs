﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ToolkitUser.Voronoi.Events
{
	public interface SiteEventListener : IEventSystemHandler
	{
		void AddSites(List<Vector3> sites);

		void AddSite(Vector3 location);
	}
}
