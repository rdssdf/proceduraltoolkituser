﻿using System;
using System.Collections.Generic;
using System.Text;
using ProceduralToolkit;
using UnityEngine;
using ToolkitUser.Architect;
using ToolkitUser.Geometry;
using UnityEngine.EventSystems;

namespace ToolkitUser.Voronoi
{
	[RequireComponent(typeof(WallGenerator))]
	public class StaticRandomVoronoiWallGenerator : MonoBehaviour
	{
		public int numberOfSites = 200;
		public float width = 1000;
		public float height = 1000;
		public int lloydRelaxations;
		public bool boundingWall;
		public Material wallMaterial;

		void Start()
		{
			ExecuteEvents.Execute<WallGenerator>(gameObject, null,
				(x, y) => x.UseMaterial(wallMaterial));

			List<Vector3> points = DrawingUtils.RandomPointsInRectangle(numberOfSites, width, height);
			Rect bounds = new Rect(0, 0, width, height);

			VoronoiEngine engine = new CsDelaunayEngine(bounds);
			VoronoiDiagram voronoi = engine.InitializeDiagram(points, lloydRelaxations);

			DisplayVoronoiDiagram(voronoi);

			if(boundingWall)
			{
				drawBoundingWall();
			}
		}

		private void drawBoundingWall()
		{
			var v0 = new Vector3(0, 0, 0);
			var v1 = new Vector3(width, 0, 0);
			var v2 = new Vector3(width, 0, height);
			var v3 = new Vector3(0, 0, height);

			DrawWall(v0, v1);
			DrawWall(v0, v3);
			DrawWall(v1, v2);
			DrawWall(v2, v3);
		}

		private void DisplayVoronoiDiagram(VoronoiDiagram voronoi)
		{
			foreach (VoronoiEdge edge in voronoi.Edges)
			{
				DrawWall(edge.From, edge.To);
			}
		}

		private void DrawWall(Vector3 from, Vector3 to)
		{
			ExecuteEvents.Execute<WallGenerator>(gameObject, null,
				(x, y) => x.Line(from, to));
		}
	}
}
