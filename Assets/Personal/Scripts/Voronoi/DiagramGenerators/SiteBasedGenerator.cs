﻿using System;
using System.Collections.Generic;
using System.Text;
using ProceduralToolkit;
using UnityEngine;
using ToolkitUser.Architect;
using ToolkitUser.Events;
using ToolkitUser.Voronoi.Events;
using UnityEngine.EventSystems;
using ToolkitUser.Geometry;

namespace ToolkitUser.Voronoi
{
	public abstract class SiteBasedGenerator : MonoBehaviour, SiteEventListener, SiteSelectionListener
	{
		public GameObject generatedMeshes;
		public Material siteMaterial;

		protected GameObject siteContainer;
		protected List<Vector3> points = new List<Vector3>();

		private SelectionHelper selectionHelper = new SelectionHelper();

		public abstract void AddSite(Vector3 location);
		public abstract void AddSites(List<Vector3> sites);

		virtual protected void Awake()
		{
			siteContainer = BuildHelper.createGameObject("sites", generatedMeshes.transform);
		}

		protected void drawSite(Vector3 location)
		{
			float radius = 2f;

			GameObject site = BuildHelper.createGameObject("site_" + (points.Count - 1), siteContainer.transform);
			site.transform.position = new Vector3(location.x, 0, location.z);
			MeshDraft siteDraft = MeshDraft.Sphere(radius, 10, 10);
			BuildHelper.ApplyMeshToGameObject(site, siteDraft);
			BuildHelper.ApplyMaterialToGameObject(site, siteMaterial);

			MeshCollider collider = site.AddComponent<MeshCollider>();
			collider.sharedMesh = siteDraft.ToMesh();

			SelectionEventProducer siteComponent = site.AddComponent<SelectionEventProducer>();
			siteComponent.addMutualExclusive(gameObject);
			siteComponent.setCallback(gameObject);
		}

		public void Selected(GameObject site)
		{
			site.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Materials/GridMaterial");
		}

		public void Deselected(GameObject site)
		{
			throw new NotImplementedException();
		}

		public void Clear()
		{
			for(int i = 0; i < siteContainer.transform.childCount; i++)
			{
				Transform t = siteContainer.transform.GetChild(i);
				t.gameObject.GetComponent<MeshRenderer>().material = siteMaterial;
			}
		}
	}

	public interface SiteSelectionListener : IEventSystemHandler
	{
		void Selected(GameObject site);
		void Deselected(GameObject site);
		void Clear();
	}

	public class SelectionEventProducer : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
	{
		private Material siteMaterial;
		private Material hoverMaterial;

		private List<GameObject> mutualExclusives = new List<GameObject>();

		private GameObject callback;

		protected void Start()
		{
			siteMaterial = GetComponent<MeshRenderer>().material;
			hoverMaterial = Resources.Load<Material>("Materials/GridMaterial");
		}

		public void addMutualExclusive(GameObject other)
		{
			mutualExclusives.Add(other);
		}

		public void setCallback(GameObject callback)
		{
			this.callback = callback;
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			foreach(GameObject other in mutualExclusives)
			{
				ExecuteEvents.Execute<MutualEventHandler>(other, null,
					(x, y) => x.SetListening(false));
			}
			GetComponent<MeshRenderer>().material = hoverMaterial;
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			foreach (GameObject other in mutualExclusives)
			{
				ExecuteEvents.Execute<MutualEventHandler>(other, null,
					(x, y) => x.SetListening(true));
			}
			GetComponent<MeshRenderer>().material = siteMaterial;
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			Debug.Log("select site at " + transform.position);
			ExecuteEvents.Execute<SiteSelectionListener>(callback, null,
				(x, y) => x.Clear());
			ExecuteEvents.Execute<SiteSelectionListener>(callback, null,
				(x, y) => x.Selected(gameObject));
		}
	}

	public class SelectionHelper
	{
		private List<GameObject> selectedObjects = new List<GameObject>();

		public void addSelection(GameObject selected)
		{
			selectedObjects.Add(selected);
		}

		public void selectSingle(GameObject selected)
		{
			selectedObjects[0] = selected;
		}

		public bool hasSelection()
		{
			return selectedObjects.Count > 0;
		}

		public bool hasMultipleSelection()
		{
			return selectedObjects.Count > 1;
		}

		public GameObject getSingleSelection()
		{
			return hasSelection() ? selectedObjects[0] : null;
		}

		public void clearSelection()
		{
			selectedObjects.Clear();
		}
	}

}
