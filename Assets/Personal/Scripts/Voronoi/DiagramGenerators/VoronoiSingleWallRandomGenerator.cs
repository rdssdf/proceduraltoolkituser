﻿using System;
using System.Collections.Generic;
using ProceduralToolkit;
using UnityEngine;
using ToolkitUser.Architect;
using ToolkitUser.Geometry;

namespace ToolkitUser.Voronoi
{
	/// <summary>
	/// ATTENTION: A mesh may not have more than 65000 vertices.
	/// Therefore this only works for a low number of sites. 2000 is too much for example.
	/// </summary>
	public class VoronoiSingleWallRandomGenerator : MonoBehaviour
	{
		public int numberOfSites = 200;
		public float boundingWidth = 1000;
		public float boundingHeight = 1000;
		public float wallWidth = 2;
		public float wallHeight = 10;
		public Material wallMaterial;
		public int lloydRelaxations = 0;

		private MeshDraft draft = new MeshDraft();

		void Start()
		{
			List<Vector3> points = DrawingUtils.RandomPointsInRectangle(numberOfSites, boundingWidth, boundingHeight);
			Rect bounds = new Rect(0, 0, boundingWidth, boundingHeight);

			VoronoiEngine engine = new CsDelaunayEngine(bounds);
			VoronoiDiagram voronoi = engine.InitializeDiagram(points, lloydRelaxations);

			DisplayVoronoiDiagram(voronoi);
		}

		private void DisplayVoronoiDiagram(VoronoiDiagram voronoi)
		{
			foreach (VoronoiEdge edge in voronoi.Edges)
			{
				DrawWall(edge);
			}

			GameObject wall = BuildHelper.createGameObject("wall_" + transform.childCount, transform);
			BuildHelper.ApplyMeshToGameObject(wall, draft);
			wall.GetComponent<MeshRenderer>().material = wallMaterial;
		}

		private void DrawWall(VoronoiEdge edge)
		{
			float factor = 0.1f;
			Vector3 from = edge.From * boundingWidth * factor;
			Vector3 to = edge.To * boundingWidth * factor;
			MeshDraft wall = MeshHelper.Wall(new WallDef(from, to, wallWidth, wallHeight));
			draft.Add(wall);
		}
	}
}
