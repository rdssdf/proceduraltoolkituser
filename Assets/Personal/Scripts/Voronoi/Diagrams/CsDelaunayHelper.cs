﻿using System;
using System.Collections.Generic;
using System.Text;
using ProceduralToolkit;
using UnityEngine;
using ToolkitUser.Architect;
using ToolkitUser.Geometry;

namespace ToolkitUser.Voronoi
{
	public static class CsDelaunayHelper
	{
		public static Vector3 Vector2to3(Vector2f v2, float height = 0)
		{
			return new Vector3(v2.x, height, v2.y);
		}

		public static List<Vector3> Rect2toList3(Rectf rect, WindingOrder order = WindingOrder.CLOCKWISE, float height = 0)
		{
			List<Vector3> result = new List<Vector3>();
			result.Add(new Vector3(rect.left, height, rect.top));
			result.Add(new Vector3(rect.right, height, rect.top));
			result.Add(new Vector3(rect.right, height, rect.bottom));
			result.Add(new Vector3(rect.left, height, rect.bottom));

			if(order == WindingOrder.COUNTERCLOCKWISE)
			{
				result.Reverse();
			}

			return result;
		}
	}
}
