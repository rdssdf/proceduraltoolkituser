﻿using System.Collections.Generic;
using UnityEngine;
using csDelaunay;
using System;
using ToolkitUser.Geometry;

namespace ToolkitUser.Voronoi
{
	class CsDelaunayDiagram : VoronoiDiagram
	{
		private csDelaunay.Voronoi voronoi;

		public CsDelaunayDiagram(csDelaunay.Voronoi voronoi)
		{
			this.voronoi = voronoi;
		}

		public IEnumerable<VoronoiEdge> Edges
		{
			get
			{
				foreach (Edge edge in voronoi.Edges)
				{
					// if the edge doesn't have clippedEnds, it was not within bounds
					if (edge.ClippedEnds == null)
						continue;

					yield return new CsDelaunayEdge(edge);
				}
			}
		}

		public IEnumerable<VoronoiSite> Sites
		{
			get
			{
				foreach (KeyValuePair<Vector2f, Site> kv in voronoi.SitesIndexedByLocation)
				{
					yield return new CsDelaunaySite(kv.Value, voronoi.PlotBounds);
				}
			}
		}
	}

	public class CsDelaunayEdge : VoronoiEdge
	{
		private Edge edge;

		public CsDelaunayEdge(Edge edge)
		{
			this.edge = edge;
		}

		public Vector3 From
		{
			get
			{
				var from = edge.ClippedEnds[LR.LEFT];
				return new Vector3(from.x, 0f, from.y);
			}
		}

		public Vector3 To
		{
			get
			{
				var to = edge.ClippedEnds[LR.RIGHT];
				return new Vector3(to.x, 0f, to.y);
			}
		}
	}

	public class CsDelaunaySite : VoronoiSite
	{
		private Site site;
		private Rectf plotBounds;

		public CsDelaunaySite(Site site, Rectf plotBounds)
		{
			this.site = site;
			this.plotBounds = plotBounds;
		}

		public IEnumerable<VoronoiEdge> Edges
		{
			get
			{
				foreach (Edge edge in site.Edges)
				{
					if (edge.ClippedEnds == null)
						continue;

					yield return new CsDelaunayEdge(edge);
				}
			}
		}

		public Vector3 Location
		{
			get
			{
				return new Vector3(site.Coord.x, 0, site.Coord.y);
			}
		}

		IEnumerable<VoronoiSite> VoronoiSite.Neighbors
		{
			get
			{
				foreach (Site neighbor in site.NeighborSites())
				{
					yield return new CsDelaunaySite(neighbor, plotBounds);
				}
			}
		}

		public List<Vector3> BoundedRegion
		{
			get
			{
				List<Vector2f> region = site.Region(plotBounds);
				List<Vector3> result = new List<Vector3>();
				foreach(Vector2f v in region)
				{
					result.Add(new Vector3(v.x, 0, v.y));
				}
				return result;
			}
		}
	}
}
