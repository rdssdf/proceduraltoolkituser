﻿using System;
using System.Collections.Generic;
using System.Text;
using ProceduralToolkit;
using UnityEngine;
using ToolkitUser.Architect;

namespace ToolkitUser.Voronoi
{
	public interface VoronoiDiagram
	{
		IEnumerable<VoronoiEdge> Edges { get; }
		IEnumerable<VoronoiSite> Sites { get; }
	}

	public interface VoronoiEdge
	{
		Vector3 From { get; }
		Vector3 To { get; }
	}

	public interface VoronoiSite
	{
		Vector3 Location { get; }
		IEnumerable<VoronoiEdge> Edges { get; }
		IEnumerable<VoronoiSite> Neighbors { get; }
		List<Vector3> BoundedRegion { get; }
	}
}
