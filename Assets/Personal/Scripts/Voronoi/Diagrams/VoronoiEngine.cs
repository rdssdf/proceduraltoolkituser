﻿using System.Collections.Generic;
using UnityEngine;

namespace ToolkitUser.Voronoi
{
	public interface VoronoiEngine
	{
		VoronoiDiagram InitializeDiagram(List<Vector3> initialSites, int lloydRelaxations);
	}

	public interface PlanarVoronoiEngine : VoronoiEngine
	{
		List<Vector3> GetBounds();
	}
}
