﻿using System;
using System.Collections.Generic;
using System.Text;
using ProceduralToolkit;
using UnityEngine;
using ToolkitUser.Architect;

namespace ToolkitUser.Voronoi
{
	public class CsDelaunayEngine : PlanarVoronoiEngine
	{
		private Rectf bounds;

		public CsDelaunayEngine(Rect bounds)
		{
			this.bounds = new Rectf(bounds.xMin, bounds.yMin, bounds.width, bounds.height);
		}

		public List<Vector3> GetBounds()
		{
			return CsDelaunayHelper.Rect2toList3(bounds);
		}

		public VoronoiDiagram InitializeDiagram(List<Vector3> points, int lloydRelaxations)
		{
			List<Vector2f> sites = convertPoints(points);
			csDelaunay.Voronoi voronoi = new csDelaunay.Voronoi(sites, bounds);
			voronoi.LloydRelaxation(lloydRelaxations);

			return new CsDelaunayDiagram(voronoi);
		}

		private List<Vector2f> convertPoints(List<Vector3> points3d)
		{
			List<Vector2f> result = new List<Vector2f>();
			foreach (Vector3 point in points3d)
			{
				result.Add(new Vector2f(point.x, point.z));
			}
			return result;
		}
	}
}
