﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Prisms
{
	public class Polygon
	{
		public Polygon(List<Vector2> vertices)
		{
			if (vertices.Count == 0)
				throw new System.ArgumentException("Cannot create polygon from empty vertex list.");

			this.vertices = vertices;

			Triangulator triangulator = new Triangulator(this.vertices.ToArray());
			m_triangleIndices = triangulator.Triangulate();
		}

		protected readonly List<Vector2> vertices;
		public List<Vector2> Vertices
		{
			get
			{
				return vertices;
			}
		}

		protected readonly int[] m_triangleIndices;
		public int[] triangleIndices
		{
			get
			{
				return m_triangleIndices;
			}
		}

		public IEnumerable<TriangleEnumerator> triangles
		{
			get
			{
				Debug.Assert(m_triangleIndices.Length % 3 == 0);

				for (int i = 0; i < m_triangleIndices.Length; i += 3)
				{
					TriangleEnumerator result = new TriangleEnumerator(vertices, i);
					yield return result;
				}
			}
		}

		public static Polygon From2D(List<Vector3> vertices)
		{
			List<Vector2> vertices2 = new List<Vector2>();
			foreach (Vector3 v3 in vertices)
			{
				Vector2 v2 = new Vector2(v3.x, v3.z);
				vertices2.Add(v2);
			}

			return new Polygon(vertices2);
		}

		#region TriangleEnumerator
		public class TriangleEnumerator
		{
			readonly List<Vector2> m_vertices;
			readonly int m_startIndex;

			public TriangleEnumerator(List<Vector2> vertices, int startIndex)
			{
				m_vertices = vertices;
				m_startIndex = startIndex;
			}

			public IEnumerable<Vector2> vertices
			{
				get
				{
					for (int i = 0; i < 3; i++)
					{
						Vector2 result = m_vertices[m_startIndex + i];
						yield return result;
					}
				}
			}
		}
		#endregion
	}

	/// <summary>
	/// A Polygon that is aware of its position in world space through its bounding rectangle.
	/// The bounding rectangle is given in world space and its center is the local origin of the polygon.
	/// The polygon's vertices are relative to its local origin, ie (0,0).
	/// </summary>
	public class WorldPolygon : Polygon
	{
		private readonly Vector2 center;
		/// <summary>
		/// The polygon's center in world space.
		/// </summary>
		public Vector2 Center
		{
			get
			{
				return center;
			}
		}

		private WorldPolygon(List<Vector2> vertices, Vector2 center)
			: base(vertices)
		{
			this.center = center;
		}

		/// <summary>
		/// Determines the bounding rectangle of the vertices and transforms the vertices
		/// into their new local space which originates at the center of the bounding rectangle.
		/// From the transformed vertices and the bounding rectangle a new WorldPolygon is created.
		/// </summary>
		/// <param name="vertices"></param>
		/// <returns></returns>
		public static WorldPolygon FromWorldSpace(List<Vector2> vertices)
		{
			if (vertices.Count == 0)
				throw new System.ArgumentException("Cannot create polygon from empty vertex list.");

			Rect boundingRectWorld = CreateBoundingRect(vertices);
			List<Vector2> centeredVertices = new List<Vector2>(vertices.Count);

			foreach (Vector2 vertex in vertices)
			{
				Vector2 relativeToBoundingRect = vertex - boundingRectWorld.center;
				centeredVertices.Add(relativeToBoundingRect);
			}

			WorldPolygon polygon = new WorldPolygon(centeredVertices, boundingRectWorld.center);
			return polygon;
		}

		public static List<Vector2> CenterVertices(List<Vector2> vertices)
		{
			if (vertices.Count == 0)
				return new List<Vector2>();

			Rect boundingRectWorld = CreateBoundingRect(vertices);
			List<Vector2> centeredVertices = new List<Vector2>(vertices.Count);

			foreach (Vector2 vertex in vertices)
			{
				Vector2 relativeToBoundingRect = vertex - boundingRectWorld.center;
				centeredVertices.Add(relativeToBoundingRect);
			}
			return centeredVertices;
		}

		public static Rect CreateBoundingRect(List<Vector2> vertices)
		{
			float minY = vertices[0].y;
			float maxY = vertices[0].y;
			float minX = vertices[0].x;
			float maxX = vertices[0].x;

			for (int i = 1; i < vertices.Count; i++)
			{
				Vector2 vertex = vertices[i];
				if (vertex.x < minX)
					minX = vertex.x;
				if (vertex.x > maxX)
					maxX = vertex.x;
				if (vertex.y < minY)
					minY = vertex.y;
				if (vertex.y > maxY)
					maxY = vertex.y;
			}

			Rect result = Rect.MinMaxRect(minX, minY, maxX, maxY);
			return result;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode() ^ center.GetHashCode();
		}

	}

}
