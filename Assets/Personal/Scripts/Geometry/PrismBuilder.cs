﻿using ProceduralToolkit;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Prisms
{
	public struct PrismDef
	{
		public readonly Polygon polygon;
		public readonly float altitude;

		public PrismDef(Polygon polygon, float altitude)
		{
			this.polygon = polygon;
			this.altitude = altitude;
		}
	}

	public enum Facing { Top, Bottom };

	public static class PrismBuilder
	{
		public static MeshDraft Prism(PrismDef prismDef)
		{
			MeshDraft result = new MeshDraft();

			float topLevel = prismDef.altitude;
			MeshDraft top = Face(prismDef.polygon, topLevel, Facing.Top);
			MeshDraft bottom = Face(prismDef.polygon, -topLevel, Facing.Bottom);
			MeshDraft walls = Walls(prismDef.polygon, prismDef.altitude);
			result.Add(top);
			result.Add(bottom);
			result.Add(walls);

			return result;
		}

		public static MeshDraft Face(Polygon polygon, float yLevel, Facing facing)
		{
			int[] indices = polygon.triangleIndices;

			Vector3[] vertices3d = new Vector3[polygon.Vertices.Count];
			for (int i = 0; i < polygon.Vertices.Count; i++)
				vertices3d[i] = new Vector3(polygon.Vertices[i].x, yLevel, polygon.Vertices[i].y);

			MeshDraft result = new MeshDraft();
			for (int i = 0; i < indices.Length; i += 3)
				result.Add(MeshDraft.Triangle(vertices3d[indices[i]], vertices3d[indices[i + 1]], vertices3d[indices[i + 2]]));

			if ((facing == Facing.Top && result.normals[0].y < 0) || (facing == Facing.Bottom && result.normals[0].y > 0))
				result.FlipFaces();

			return result;
		}

		public static MeshDraft Walls(Polygon polygon, float height)
		{
			MeshDraft result = new MeshDraft();
			List<Vector2> vertices = polygon.Vertices;
			for (int i = 0; i < vertices.Count - 1; i++)
				result.Add(Wall(vertices[i], vertices[i + 1], height));
			result.Add(Wall(vertices[vertices.Count - 1], vertices[0], height));

			return result;
		}

		public static MeshDraft Wall(Vector2 pointA, Vector2 pointB, float height)
		{
			float length = Vector2.Distance(pointA, pointB);
			MeshDraft result = CreateWall(length, height);

			// determine the angle between the x axis and the wall
			Vector2 direction = pointB - pointA;
			float angle = Vector2.Angle(Vector2.right, direction);

			//print("create wall from " + pointA + " to " + pointB + ". length: " + length + ", direction: " + direction + ", angle from x-axis to wall: " + angle);

			// adjust the angle based on... trial and error ;)
			if (direction.y <= 0)
				angle += 180;
			else
				angle = 180 - angle;

			result.Rotate(Quaternion.Euler(Vector3.up * angle));

			// translate the rectangle to the correct position
			result.Move(new Vector3(pointB.x, height, pointB.y));

			return result;
		}

		static MeshDraft CreateWall(float width, float height)
		{
			// create a 2D rectangle aligned on the x axis
			Vector2[] rectangle = Rectangle2d(width, -height * 2);

			// convert to 3D
			List<Vector3> vertices3d = new List<Vector3>(rectangle.Length);
			foreach (Vector2 vertex2d in rectangle)
				vertices3d.Add(new Vector3(vertex2d.x, 0, vertex2d.y));

			// triangulate
			Triangulator triangulator = new Triangulator(rectangle);
			int[] indices = triangulator.Triangulate();

			// create MeshDraft from 3D vertices
			MeshDraft result = new MeshDraft();
			for (int i = 0; i < indices.Length; i += 3)
				result.Add(MeshDraft.Triangle(vertices3d[indices[i]], vertices3d[indices[i + 1]], vertices3d[indices[i + 2]]));

			// rotate 90 degrees around x axis
			result.Rotate(Quaternion.Euler(Vector3.right * 90));

			return result;
		}

		static Vector2[] Rectangle2d(float width, float height)
		{
			return new Vector2[4]
			{
				new Vector2(0,0),
				new Vector2(width, 0),
				new Vector2(width, -height),
				new Vector2(0, -height)
			};
		}
	}
}
